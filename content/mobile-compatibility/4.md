+++
title = "Mobile Compatibility: 4"
description = "It's 4 out of 5"
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++
Almost perfect, works fine with tweaks for scaling, like <code>scale-to-fit</code> on Phosh.