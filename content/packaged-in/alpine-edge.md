+++
title = "Alpine edge"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

Alpine edge is the rolling branch of [Alpine Linux](https://www.alpinelinux.org/). It's used in [postmarketOS edge](../postmarketos-master), which may contains postmarketOS/mobile specific packages (mainly device kernels, firmware and similar things).
