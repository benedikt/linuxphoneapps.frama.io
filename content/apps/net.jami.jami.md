+++
title = "Jami"
description = "jami-qt is the cross platform client for Jami."
aliases = []
date = 2022-04-26

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "savoirfairelinux",]
categories = [ "chat", "telephony",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "QtQuick 6",]
backends = []
services = [ "Jami", "SIP",]
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Network", "Telephony", "Chat",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://git.jami.net/savoirfairelinux/jami-client-qt"
homepage = "https://jami.net/"
bugtracker = ""
donations = "https://www.paypal.com/donate/?hosted_button_id=MGUDJLQZ4TP5W"
translations = "/savoirfairelinux/jami/"
more_information = [ "https://jami.net/help/",]
summary_source_url = "https://git.jami.net/savoirfairelinux/jami-client-qt"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/net.jami.jami/1.png", "https://img.linuxphoneapps.org/net.jami.jami/2.png", "https://img.linuxphoneapps.org/net.jami.jami/3.png", "https://img.linuxphoneapps.org/net.jami.jami/4.png", "https://img.linuxphoneapps.org/net.jami.jami/5.png", "https://img.linuxphoneapps.org/net.jami.jami/6.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.jami.Jami"
scale_to_fit = "net.jami.jami-qt"
flathub = "https://flathub.org/apps/net.jami.Jami"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "jami-qt",]
appstream_xml_url = "https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml"
reported_by = "linmob"
updated_by = ""
+++





### Description

Privacy-oriented voice, video, chat, and conference platform. An end-to-end encrypted secure and distributed voice, video, and chat communication platform that requires no central server and leaves the power of privacy and freedom in the hands of users. Jami supports the following key features: - One-to-one conversations, - File sharing, - Audio calls and conferences, - Video calls and conferences, - Screen sharing in video calls and conferences, - Recording and sending audio messages, - Recording and sending video messages,  -Functioning as a SIP phone software [Source](https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml)


### Notice

Arch ARM and flatpak builds were evaluated. It fits the screen mostly okay without scale-to-fit for the chat and phone UI after setting [recommended env vars](https://img.linuxphoneapps.org/net.jami.jami/flatseal.png) (you won't see what you type), scale-to-fit makes the app behave worse in many circumstances. Voice calls work, the button to hang up is invisible, just tap the center of the lower half, chat while video/audio makes the window far two wide (somewhat usable in landscape). Video calls don't work out of the box but may work after setting up camera/video pipelines properly.
