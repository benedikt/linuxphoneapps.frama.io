+++
title = "Anki"
description = "Powerful, intelligent flash cards. Remembering things just became much easier."
aliases = []
date = 2020-12-19
updated = 2022-04-11

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Damien Elmes",]
categories = [ "education",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_unstable", "devuan_4_0", "flathub", "gentoo", "gnuguix", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "Qt", "Education",]
programming_languages = [ "Python", "Rust", "TypeScript",]
build_systems = [ "custom",]

[extra]
repository = "https://github.com/ankitects/anki"
homepage = "https://apps.ankiweb.net/"
bugtracker = "https://anki.tenderapp.com/discussions/ankidesktop"
donations = ""
translations = ""
more_information = [ "https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/268",]
summary_source_url = "https://apps.ankiweb.net/"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/net.ankiweb.anki/1.png", "https://img.linuxphoneapps.org/net.ankiweb.anki/2.png", "https://img.linuxphoneapps.org/net.ankiweb.anki/3.png", "https://img.linuxphoneapps.org/net.ankiweb.anki/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.ankiweb.Anki"
scale_to_fit = "anki"
flathub = "https://flathub.org/apps/net.ankiweb.Anki"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "anki",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/net.ankiweb.Anki/master/net.ankiweb.Anki.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++







### Notice

Because of added complexity (requiring a number of language bindings and build systems), distribution packaging of Anki on ARM/aarch64 may be outdated, even on Flathub.
