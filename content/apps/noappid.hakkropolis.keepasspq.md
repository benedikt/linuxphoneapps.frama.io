+++
title = "KeePassPQ"
description = "KeePassPQ is a Python and QML based application to open KeePass (v4) databases on mobile devices."
aliases = []
date = 2022-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "hakkropolis",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = [ "pykeepass",]
services = [ "KeePass",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://gitlab.com/hakkropolis/keepasspq"
homepage = "https://hakkropolis.org/"
bugtracker = "https://gitlab.com/hakkropolis/keepasspq/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://hakkropolis.org/"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/noappid.hakkropolis.keepasspq/1.png", "https://img.linuxphoneapps.org/noappid.hakkropolis.keepasspq/2.png", "https://img.linuxphoneapps.org/noappid.hakkropolis.keepasspq/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

KeePassPQ is a Python and QML based application to open KeePass (v4) databases on mobile devices. It mainly targets the kde plasma environment (pinephone) and uses the pykeepass library. Currently this application is fairly limited and more of a work in progress until another Qt/Qml based convergent app is available or a higher maturity is reached. [Source](https://gitlab.com/hakkropolis/keepasspq)


### Notice

The repo includes a PKGBUILD file to build a package for Arch/Manjaro, and an unofficial [APKBUILD](https://framagit.org/linmobapps/apkbuilds/-/tree/master/keepasspq) for Alpine/postmarketOS is available, too.