+++
title = "Strike"
description = "Strike is a simple minimal IDE for the Linux phones. Code, build, and run from the phone."
aliases = []
date = 2022-01-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Development", "IDE",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/strike"
homepage = "https://mauikit.org"
bugtracker = "https://invent.kde.org/maui/strike/-/issues/"
donations = ""
translations = ""
more_information = [ "https://mauikit.org/blog/maui-report-13/",]
summary_source_url = "https://invent.kde.org/maui/strike"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.strike"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-strike",]
appstream_xml_url = "https://invent.kde.org/maui/strike/-/raw/master/org.kde.strike.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++



