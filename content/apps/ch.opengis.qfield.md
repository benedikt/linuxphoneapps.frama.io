+++
title = "QField"
description = "A simplified touch optimized interface for QGIS. Perfect for field work on portable touch devices."
aliases = []
date = 2019-03-19
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "opengisch",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Geography", "Maps",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/opengisch/QField"
homepage = "https://www.qfield.org/"
bugtracker = "https://github.com/opengisch/QField/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/opengisch/QField"
screenshots = [ "https://play.google.com/store/apps/details?id=ch.opengis.qfield",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "ch.opengis.qfield"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/opengisch/QField/master/platform/linux/qfield.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
