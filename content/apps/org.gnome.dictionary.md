+++
title = "Dictionary"
description = "Check word definitions and spellings in an online dictionary"
aliases = []
date = 2020-10-15
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.0-or-later", "GFDL-1.3",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emmanuele Bassi",]
categories = [ "dictionary",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/Archive/gnome-dictionary"
homepage = "https://wiki.gnome.org/Apps/Dictionary"
bugtracker = ""
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/Archive/gnome-dictionary"
screenshots = [ "https://gitlab.gnome.org/Archive/gnome-dictionary/-/raw/master/data/appdata/gnome-dictionary-main.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Dictionary"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-dictionary",]
appstream_xml_url = "https://gitlab.gnome.org/Archive/gnome-dictionary/-/raw/master/data/appdata/org.gnome.Dictionary.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"
+++





### Description

GNOME Dictionary is a simple, clean, elegant application to look up words in online dictionaries using the DICT protocol. [Source](https://gitlab.gnome.org/Archive/gnome-dictionary)

### Notice

Archived in November 2022.
