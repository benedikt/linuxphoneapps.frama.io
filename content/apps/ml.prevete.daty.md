+++
title = "Daty"
description = "cross-platform advanced Wikidata editor"
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "Pellegrino Prevete",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Wikidata",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Daty"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Daty/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Daty"
screenshots = [ "https://gitlab.gnome.org/World/Daty",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "ml.prevete.Daty"
scale_to_fit = "daty"
flathub = "https://flathub.org/apps/ml.prevete.Daty"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "daty",]
appstream_xml_url = "https://gitlab.gnome.org/World/Daty/-/raw/master/flatpak/ml.prevete.Daty.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++





### Description

Daty is a free cross-platform advanced Wikidata editor adhering to GNOME Human Interface Guidelines, intended to enable better editing workflow and faster deployment of requested user features. Use Daty to search, select, read, batch edit items, script actions, share, visualize proposed changes and bots. [Source](https://gitlab.gnome.org/World/Daty)


### Notice

Only a few pixels to wide.
