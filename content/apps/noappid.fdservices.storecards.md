+++
title = "Store Cards"
description = "Keep all your store cards on your linux phone"
aliases = []
date = 2022-01-23
updated = 2023-07-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "fdservices",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Tk",]
backends = [ "zint",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Tcl",]
build_systems = [ "none",]

[extra]
repository = "https://github.com/fdservices/storecards"
homepage = ""
bugtracker = "https://github.com/fdservices/storecards/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/fdservices/storecards"
screenshots = [ "https://user-images.githubusercontent.com/3006039/254495837-f5562f96-ec8b-4d9b-84b8-4c71c6a1d337.png", "https://user-images.githubusercontent.com/3006039/254495891-f7f2e043-c2fd-4d9e-b6af-acc5b68aba19.png", "https://user-images.githubusercontent.com/3006039/254497191-ac6e1144-4b9d-4693-ab69-8fa6aff34688.png"]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "storecards",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

Store Cards is built using Tcl/TK and Zint
The aim is to keep all your store cards on your linux phone.

### Notice

Simple app to store membership/coupon cards - enter a number and it generates a barcode.
