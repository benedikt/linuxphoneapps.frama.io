+++
title = "Metronome"
description = "A simple Metronome application for the GNOME desktop"
aliases = []
date = 2020-10-21
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "jvieira.tpt",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_unstable", "devuan_4_0",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Music",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/a-wai/metronome"
homepage = "https://gitlab.gnome.org/a-wai/metronome"
bugtracker = "https://gitlab.gnome.org/a-wai/metronome/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/a-wai/metronome/-/raw/master/data/com.jvieira.tpt.Metronome.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/a-wai/metronome/raw/master/data/screenshots/metronome.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.jvieira.tpt.Metronome"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/a-wai/metronome/-/raw/master/com.jvieira.tpt.Metronome.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-metronome",]
appstream_xml_url = "https://gitlab.gnome.org/a-wai/metronome/-/raw/master/data/com.jvieira.tpt.Metronome.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++




### Notice

Original repo is gone, code has not been updated since 2020-06-15.
