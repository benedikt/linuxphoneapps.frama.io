+++
title = "FluffyChat"
description = "Chat with your friends"
aliases = []
date = 2021-05-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "fluffychat",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Flutter",]
backends = [ "Matrix",]
services = [ "Matrix",]
packaged_in = [ "aur", "flathub", "nix_stable_22_11",]
freedesktop_categories = [ "Network", "InstantMessaging", "Chat",]
programming_languages = [ "Dart",]
build_systems = [ "cmake", "ninja",]

[extra]
repository = "https://gitlab.com/famedly/fluffychat/"
homepage = "https://fluffychat.im/"
bugtracker = "https://gitlab.com/famedly/fluffychat/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/famedly/fluffychat/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "im.fluffychat.Fluffychat"
scale_to_fit = ""
flathub = "https://flathub.org/apps/im.fluffychat.Fluffychat"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "fluffychat",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++





### Description

FluffyChat is a multi-platform Matrix client written in Dart/Flutter. It compiles to native code von Android, iOS, macOS, Windows and Linux and renders with Skia on the web. [Source](https://gitlab.com/famedly/fluffychat/)
