+++
title = "Phosh Mobile Settings"
description = "Mobile Settings App for phosh and related components."
aliases = []
date = 2022-06-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "guido.gunther",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_unstable", "postmarketos_22.12", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/guido.gunther/phosh-mobile-settings"
homepage = ""
bugtracker = "https://source.puri.sm/guido.gunther/phosh-mobile-settings/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/guido.gunther/phosh-mobile-settings"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/1.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/2.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/3.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/4.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/5.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.sigxcpu.MobileSettings"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "phosh-mobile-settings",]
appstream_xml_url = "https://source.puri.sm/guido.gunther/phosh-mobile-settings/-/raw/pureos/byzantium/data/org.sigxcpu.MobileSettings.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++






### Description

This app allows you to configure some aspects of Phosh that would otherwise require command line usage. [Source](https://source.puri.sm/guido.gunther/phosh-mobile-settings/-/raw/pureos/byzantium/data/org.sigxcpu.MobileSettings.metainfo.xml.in)


### Notice

No more manual fiddling with feedbackd and no more APP ID guessing for scale-to-fit. Note: Release 0.21.0 does not fit the screen, which can be fixed in packaging/before building by applying [this patch](https://sources.debian.org/data/main/p/phosh-mobile-settings/0.21.0-2/debian/patches/Revert-lockscreen-panel-Mark-additional-widgets-as-experi.patch).