+++
title = "Podcasts"
description = "Listen to your favorite shows"
aliases = []
date = 2019-02-16
updated = 2023-07-12

[taxonomies]
project_licenses = [ "GPL-3.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Julian Hofer",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "Audio", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/podcasts"
homepage = "https://wiki.gnome.org/Apps/Podcasts"
bugtracker = "https://gitlab.gnome.org/World/podcasts/issues/"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Podcasts/",]
summary_source_url = "https://gitlab.gnome.org/World/podcasts"
screenshots = [ "https://gitlab.gnome.org/World/podcasts/raw/master/screenshots/home_view.png", "https://gitlab.gnome.org/World/podcasts/raw/master/screenshots/shows_view.png", "https://gitlab.gnome.org/World/podcasts/raw/master/screenshots/show_widget.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.podcasts/1.png", "https://img.linuxphoneapps.org/org.gnome.podcasts/2.png", "https://img.linuxphoneapps.org/org.gnome.podcasts/3.png", "https://img.linuxphoneapps.org/org.gnome.podcasts/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Podcasts"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Podcasts"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-podcasts",]
appstream_xml_url = "https://gitlab.gnome.org/World/podcasts/-/raw/master/podcasts-gtk/resources/org.gnome.Podcasts.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "linmob"
+++


### Description

Play, update, and manage your podcasts from a lightweight interface that seamlessly integrates with GNOME. Podcasts can play various audio formats and remember where you stopped listening. You can subscribe to shows via RSS/Atom, iTunes, and Soundcloud links. Subscriptions from other apps can be imported via OPML files. 

[Source](https://gitlab.gnome.org/World/podcasts/-/raw/master/podcasts-gtk/resources/org.gnome.Podcasts.appdata.xml.in.in)

### Notice

Was GTK3/libhandy before release 0.6.0.

