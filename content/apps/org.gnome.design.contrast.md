+++
title = "Contrast"
description = "Checks whether the contrast between two colors meet the WCAG requirements."
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Bilal Elmoussaoui",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/design/contrast"
homepage = "https://apps.gnome.org/app/org.gnome.design.Contrast/"
bugtracker = "https://gitlab.gnome.org/World/design/contrast/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/design/contrast"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.design.Contrast"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.design.Contrast"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "contrast",]
appstream_xml_url = "https://gitlab.gnome.org/World/design/contrast/-/raw/master/data/org.gnome.design.Contrast.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++



