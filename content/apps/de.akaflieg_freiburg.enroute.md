+++
title = "Enroute"
description = "Enroute Flight Navigation is a mobile flight navigation app for Android and other devices."
aliases = []
date = 2020-12-20
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "akaflieg-freiburg",]
categories = [ "flight navigation",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/Akaflieg-Freiburg/enroute"
homepage = "https://akaflieg-freiburg.github.io/enroute/"
bugtracker = "https://github.com/Akaflieg-Freiburg/enroute/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Akaflieg-Freiburg/enroute"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.akaflieg_freiburg.enroute"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.akaflieg_freiburg.enroute"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "enroute",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++




