+++
title = "Teleport"
description = "Send files on the local network"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "file transfer",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "FileTransfer", "P2P",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/jsparber/teleport"
homepage = ""
bugtracker = "https://gitlab.gnome.org/jsparber/teleport/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/jsparber/teleport"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.frac_tion.teleport"
scale_to_fit = "teleport"
flathub = "https://flathub.org/apps/com.frac_tion.teleport"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "teleport-share",]
appstream_xml_url = "https://gitlab.gnome.org/jsparber/teleport/-/raw/master/data/com.frac_tion.teleport.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++






### Notice

Flatpak release is great after scale to fit, later versions seem to have libhandy (test required), requires avahi.
