+++
title = "Telly Skout"
description = "Convergent TV guide based on Kirigami."
aliases = []
date = 2021-08-14
updated = 2023-02-15

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "plata",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = [ "xmltv.se",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/telly-skout"
homepage = "https://apps.kde.org/telly-skout"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Telly%20Skout"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/telly-skout"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.telly-skout/1.png", "https://img.linuxphoneapps.org/org.kde.telly-skout/2.png", "https://img.linuxphoneapps.org/org.kde.telly-skout/3.png", "https://img.linuxphoneapps.org/org.kde.telly-skout/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.telly-skout"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "telly-skout",]
appstream_xml_url = "https://invent.kde.org/utilities/telly-skout/-/raw/master/org.kde.telly-skout.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++





### Description

Telly Skout is a convergent Kirigami TV guide. It shows the TV program for your favorite channels from TV Spielfilm or an XMLTV file. [Source](https://invent.kde.org/utilities/telly-skout)
