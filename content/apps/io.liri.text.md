+++
title = "Liri Text"
description = "Liri Text is a cross-platform, Material Design text editor, currently in  alpha  stage. "
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The Liri developers",]
categories = [ "text editor",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility", "TextEditor",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/text"
homepage = "https://liri.io/apps/text/"
bugtracker = "https://github.com/lirios/text/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://liri.io/apps/text/"
screenshots = [ "https://liri.io/apps/text/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.Text"
scale_to_fit = "io.liri.Text"
flathub = "https://flathub.org/apps/io.liri.Text"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "liri-text",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/text/develop/data/io.liri.Text.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++



