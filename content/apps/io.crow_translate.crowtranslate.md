+++
title = "Crow Translate"
description = "A simple and lightweight translator that allows to translate and speak text using Google, Yandex and Bing."
aliases = []
date = 2021-02-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "crow-translate",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = [ "Google Translate", "bing translate",]
packaged_in = [ "aur", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/crow-translate/crow-translate"
homepage = ""
bugtracker = "https://github.com/crow-translate/crow-translate/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/crow-translate/crow-translate"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.crow_translate.CrowTranslate"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "crow-translate",]
appstream_xml_url = "https://raw.githubusercontent.com/crow-translate/crow-translate/master/data/io.crow_translate.CrowTranslate.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




