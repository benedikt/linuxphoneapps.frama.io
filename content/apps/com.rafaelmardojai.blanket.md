+++
title = "Blanket"
description = "Listen to different sounds"
aliases = []
date = 2020-09-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Rafael Mardojai CM",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/rafaelmardojai/blanket"
homepage = ""
bugtracker = "https://github.com/rafaelmardojai/blanket/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.rafaelmardojai.Blanket/",]
summary_source_url = "https://github.com/rafaelmardojai/blanket"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.rafaelmardojai.Blanket"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.rafaelmardojai.Blanket"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "blanket",]
appstream_xml_url = "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




