+++
title = "News Flash"
description = "A modern feed reader designed for the GNOME desktop."
aliases = []
date = 2020-08-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan Lukas Gernert",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Miniflux", "feedly", "feedbin", "RSS",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/news-flash/news_flash_gtk/"
homepage = ""
bugtracker = "https://gitlab.com/news-flash/news_flash_gtk/-/issues/"
donations = ""
translations = "https://hosted.weblate.org/projects/newsflash/news_flash_gtk/"
more_information = [ "https://apps.gnome.org/app/com.gitlab.newsflash/", "https://wiki.mobian-project.org/doku.php?id=newsflash", "https://linmob.net/2020/07/31/pinephone-daily-driver-challenge-part3-reading-apps-and-email.html#newsflash",]
summary_source_url = "https://gitlab.com/news-flash/news_flash_gtk/"
screenshots = [ "https://gitlab.com/news-flash/news_flash_gtk/-/raw/master/data/screenshots/Adaptive.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.gitlab.newsflash/1.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/2.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/3.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/4.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/5.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/6.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/7.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/8.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.newsflash"
scale_to_fit = "NewsflashGTK"
flathub = "https://flathub.org/apps/com.gitlab.newsflash"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "newsflash",]
appstream_xml_url = "https://gitlab.com/news-flash/news_flash_gtk/-/raw/master/data/com.gitlab.newsflash.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++





### Description

A modern feed reader designed for the GNOME desktop. NewsFlash is a program designed to complement an already existing web-based RSS reader account. [Source](https://gitlab.com/news-flash/news_flash_gtk/)


### Notice

Scales fine. Was GTK3/libhandy before release 2.0.
