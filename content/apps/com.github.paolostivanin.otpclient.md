+++
title = "OTPClient"
description = "GTK+ application for managing TOTP and HOTP tokens with built-in encryption."
aliases = []
date = 2021-07-24
updated = 2023-05-04

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-4.0-only",]
app_author = [ "Paolo Stivanin",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = [ "libcotp",]
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "flathub", "gnuguix", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/paolostivanin/OTPClient"
homepage = "https://github.com/paolostivanin/OTPClient"
bugtracker = "https://github.com/paolostivanin/OTPClient/issues"
donations = ""
translations = ""
more_information = [ "https://github.com/paolostivanin/OTPClient/wiki",]
summary_source_url = "https://github.com/paolostivanin/OTPClient"
screenshots = [ "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/emptymain.png", "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/addmenu.png", "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/hambmenu.png", "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/settings.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.paolostivanin.OTPClient.desktop"
scale_to_fit = "otpclient"
flathub = "https://flathub.org/apps/com.github.paolostivanin.OTPClient"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "otpclient",]
appstream_xml_url = "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/com.github.paolostivanin.OTPClient.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Highly secure and easy to use GTK+ software for two-factor authentication that supports both Time-based One-time Passwords (TOTP) and HMAC-Based One-Time Passwords (HOTP). [Source](https://github.com/paolostivanin/OTPClient)


### Notice

Despite being 'intended for mobile' according to its metadata, release 3.1.6 from Flathub is far too wide to work great on mobile, while the Alpine package of the same release fits just fine.
Supports importing from many other OTP apps.
