+++
title = "Scrum Poker"
description = "A small gtk app to help estimate the effort for a task."
aliases = []
date = 2021-05-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "chefe",]
categories = [ "utilities", "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "make", "cargo",]

[extra]
repository = "https://github.com/chefe/scrumpoker"
homepage = ""
bugtracker = "https://github.com/chefe/scrumpoker/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/chefe/scrumpoker"
screenshots = [ "https://pbs.twimg.com/media/E2ppFCtXwAYVQWL?format=jpg&name=large",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.chefe.scrumpoker"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++