+++
title = "DieBahn"
description = "Search for Trains"
aliases = []
date = 2022-03-22
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "hafas-rest",]
services = [ "hafas",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]

[extra]
repository = "https://gitlab.com/Schmiddiii/diebahn"
homepage = ""
bugtracker = "https://gitlab.com/Schmiddiii/diebahn/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/Schmiddiii/diebahn/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml"
screenshots = [ "https://gitlab.com/Schmiddiii/diebahn/-/raw/master/data/screenshots/overview.png",]
screenshots_img = []
all_features_touch = 1
intended_for_mobile = 1
app_id = "de.schmidhuberj.DieBahn"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.DieBahn"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/Schmiddiii/diebahn/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Description

A GTK4 frontend for the travel information of railways.

*   Search for a journey
*   List all available journeys
*   View the details of a journey including departure, arrivals, delays, platforms
*   Bookmark journey
*   Convergent for small screens
[Source](https://gitlab.com/Schmiddiii/diebahn/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml)
