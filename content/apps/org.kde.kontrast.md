+++
title = "Kontrast"
description = "Tool to check contrast for colors that allows verifying that your colors are correctly accessible"
aliases = []
date = 2020-09-10
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Carl Schwan",]
categories = [ "accessibility",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/accessibility/kontrast"
homepage = ""
bugtracker = "https://invent.kde.org/accessibility/kontrast/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/accessibility/kontrast"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kontrast"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kontrast"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kontrast",]
appstream_xml_url = "https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




