+++
title = "Half Life"
description = "Sci-fi first-person shooter developed and published by Valve."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "Proprietary",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = []
backends = []
services = []
packaged_in = []

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/ValveSoftware/halflife"
homepage = "https://half-life.fandom.com/wiki/Half-Life"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++