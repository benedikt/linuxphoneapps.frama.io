+++
title = "Endless Sky"
description = "Space exploration, trading, and combat game."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/endless-sky/endless-sky"
homepage = "https://endless-sky.github.io/"
more_information = []
summary_source_url = "https://github.com/endless-sky/endless-sky"
screenshots = []
screenshots_img = []
app_id = "io.github.endless_sky.endless_sky"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.endless_sky.endless_sky"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "endless-sky",]
appstream_xml_url = "https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml"
+++



