import os
import sys
import requests
import toml
import glob

def update_packaged_in(folder_path):
    md_files = glob.glob(os.path.join(folder_path, "*.md"))
    for md_file in md_files:
        with open(md_file, "r") as f:
            content = f.read()
            frontmatter_start = content.find("+++")
            frontmatter_end = content.find("+++", frontmatter_start + 3)
            frontmatter = content[frontmatter_start + 3:frontmatter_end].strip()

            try:
                toml_data = toml.loads(frontmatter)
            except toml.decoder.TomlDecodeError as e:
                print(f"Error decoding frontmatter in file {md_file}: {e}")
                continue

            if "repology" not in toml_data["extra"]:
                continue

            package_names = toml_data["extra"]["repology"]
            if not package_names:
                continue

            packaged_in = []
            for package_name in package_names:
                api_url = f"https://repology.org/api/v1/project/{package_name}"
                response = requests.get(api_url)

                if response.status_code != 200:
                    print(f"Failed to fetch data for package {package_name} from Repology API. HTTP status code: {response.status_code}")
                    continue

                data = response.json()
                if not data:
                    print(f"No data returned for package {package_name} from Repology API")
                    continue

                for d in data:
                    repo = d.get("repo")
                    if repo in valid_distributions:
                        packaged_in.append(repo)

            # Check flathub and snapcraft URLs
            flathub_url = toml_data["extra"].get("flathub")
            if flathub_url:
                if requests.head(flathub_url).status_code == 200:
                    packaged_in.append("flathub")

            snapcraft_url = toml_data["extra"].get("snapcraft")
            if snapcraft_url:
                if requests.head(snapcraft_url).status_code == 200:
                    packaged_in.append("snapcraft")

            # remove duplicates
            clean_packaged_in = list(set(packaged_in))

            # sort the list
            toml_data["taxonomies"]["packaged_in"] = sorted(clean_packaged_in)

            # Preserve markdown data after frontmatter
            md_content = content[frontmatter_end + 3:]
            with open(md_file, "w") as f:
                f.write(f"+++\n{toml.dumps(toml_data)}+++\n{md_content}")
                print(f"Updated {md_file}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please provide a folder path as the first argument")
        sys.exit(1)

    folder_path = sys.argv[1]

    # List of valid distributions
    valid_distributions = [
        "alpine_3_17",
        "alpine_3_18",
        "alpine_edge",
        "archlinuxarm_aarch64",
        "archlinuxarm_armv7h",
        "aur",
        "debian_11",
        "debian_12",
        "debian_13",
        "debian_unstable",
        "debian_experimental",
        "devuan_4_0",
        "fedora_38",
        "fedora_rawhide",
        "gentoo",
        "gnuguix",
        "nix_stable_22_11",
        "nix_stable_23_05",
        "nix_unstable", 
        "manjaro_stable",
        "manjaro_unstable",
        "opensuse_tumbleweed",
        "postmarketos_master",
        "postmarketos_23.06",
        "postmarketos_22.12",
        "pureos_landing",
    ]

    update_packaged_in(folder_path)

